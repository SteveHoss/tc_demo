package patches.buildTypes

import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.BuildType
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs
import jetbrains.buildServer.configs.kotlin.v2019_2.ui.*

/*
This patch script was generated by TeamCity on settings change in UI.
To apply the patch, create a buildType with id = 'ExampleRepo2_Build'
in the project with id = 'ExampleRepo2', and delete the patch script.
*/
create(RelativeId("ExampleRepo2"), BuildType({
    id("ExampleRepo2_Build")
    name = "Build"

    allowExternalStatus = true

    vcs {
        root(RelativeId("ExampleRepo2_HttpsGitlabComSteveHossExampleRepo2gitRefsHeadsMaster"))
    }

    steps {
        script {
            name = "My Echo"
            scriptContent = """echo "Hello Steve""""
        }
    }

    triggers {
        vcs {
        }
    }

    dependencies {
        snapshot(RelativeId("Build")) {
        }
    }
}))

