import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.dockerCommand
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.triggers.vcs

version = "2020.1"

project {

    buildType(Build)
}

object Build : BuildType({
    name = "Build"
    description = "abc 123"
    id("Build")

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {

        script {
            name = "Set version using script"
            scriptContent = """
      #!/bin/bash
      HASH=%build.vcs.number%
      SHORT_HASH=${"$"}{HASH:0:7}
      BUILD_COUNTER=%build.counter%
      BUILD_NUMBER="1.0${"$"}BUILD_COUNTER.${"$"}SHORT_HASH"
      echo "##teamcity[buildNumber '${"$"}BUILD_NUMBER']"
      """.trimIndent()
        }
        script {
            name = "build"
            scriptContent = """
      mkdir bin
      echo "built artifact" > bin/compiled.txt
      """.trimIndent()
        }
        dockerCommand {
            name = "Build Dockerfile"
            commandType = build {
                source = content {
                    content = """
                            FROM ubuntu 
                            CMD [“echo”,”I built and image from TeamCity!”]
                        """.trimIndent()
                }
                namesAndTags = "192.168.99.106:5000/tc-build-18-20"
                commandArgs = "--pull"
            }
        }
        dockerCommand {
            name = "Push Dockerfile"
            commandType = push {
                namesAndTags = "192.168.99.106:5000/tc-build-18-20"
            }
        }
    }

    triggers {
        vcs {
        }
    }
})
